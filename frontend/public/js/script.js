$(".owl-carousel").owlCarousel({
  loop: true,
  margin: 32,
  center: true,
  mergeFit: false,
  autoWidth: true,
  nav: true,
  dots: false,
  navText: [
    '<img style="margin: 24px 12px 0px 12px;" src="./assets/left_button.svg" alt="leftbutton">',
    '<img style="margin: 24px 12px 0px 12px;" src="./assets/right_button.svg" alt="rightbutton">',
  ],
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 3,
    },
  },
});

var acc = document.getElementsByClassName("faqCard");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}
