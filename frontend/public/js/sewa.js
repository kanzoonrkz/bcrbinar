// api url
const api_url = env("API_URL");

var cars;

fetch(api_url)
  .then((response) => response.json())
  .then((data) => (cars = data));

function showResult(cars) {
  cars.forEach(function (item, i) {
    var cardComponent = "<h1>" + item.plate + "</h1>";
    document.getElementById("results").innerHTML += cardComponent;
  });
}

function getDriver() {
  const dropdown = document.getElementById("driver");
  var value = dropdown.options[dropdown.selectedIndex].value;
  console.log(value);
}

function getDate() {
  const value = document.getElementById("date").value;
  console.log(value);
}

function getTime() {
  const value = document.getElementById("time").value;
  console.log(value);
}

function getCount() {
  const value = document.getElementById("count").value;
  console.log(value);
}
