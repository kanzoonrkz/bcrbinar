const express = require("express");
const app = express();
const PORT = process.env.port || 8000;
const path = require("path");
const fs = require("fs");
let ejs = require("ejs");

app.use(express.static("public"));
app.use("/assets", express.static(__dirname + "public/assets"));
app.use("/styles", express.static(__dirname + "public/styles"));
app.use("/js", express.static(__dirname + "public/js"));

app.set("views", "./pages");
app.set("view engine", "ejs");

app.get("/", (req, res) => {
  res.render("index");
});

app.get("/sewa", (req, res) => {
  res.render("sewa");
});

app.get("/dashboard/cars", (req, res) => {
  res.render("adminCars");
});

app.get("/cars", (req, res) => {
  let rawData = fs.readFileSync(path.join(__dirname + "/data/cars.json"));
  res.json(JSON.parse(rawData));
});

app.listen(PORT, () => console.log(`Server listening on port: ${PORT}`));

module.exports = app;
