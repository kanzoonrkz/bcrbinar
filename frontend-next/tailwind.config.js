module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily:{},
      colors: {
        BLUE: "#0D28A6",
        BLUEsoft: "#F1F3FF",
        GREEN: "#5CB85F",
        GREENdark: "#3D7B3F",
      },
    },
  },
  plugins: [],
}
