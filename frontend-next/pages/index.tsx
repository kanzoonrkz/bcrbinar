import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import HeroSection from "../components/heroSection";
import MainLayout from "../components/layout/mainLayout";
import QnaAccordion from "../components/qnaAccordion";

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>BCR</title>
        <meta name="description" content="Rent A Car Anywhere Anytime" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <MainLayout>
        <HeroSection ishome={true}/>

        <section
          id="OurServices"
          className="max-w-screen-xl mx-auto py-3 px-3 grid grid-cols-1 md:grid-cols-2 gap-3"
        >
          <div className="flex-cc p-5">
            <img src="img_service.png" alt="our service" />
          </div>
          <div className="col flex-cs gap-3">
            <h2>Best Car Rental for any kind of trip in Indonesia!</h2>
            <p>
              Sewa mobil di Indonesia bersama Binar Car Rental jaminan harga
              lebih murah dibandingkan yang lain, kondisi mobil baru, serta
              kualitas pelayanan terbaik untuk perjalanan wisata, bisnis,
              wedding, meeting, dll.
            </p>
            <ul className="pt-3">
              {[
                "Sewa Mobil Dengan Supir di seluruh Indonesia 12 Jam",
                "Sewa Mobil Lepas Kunci di seluruh Indonesia 24 Jam",
                "Sewa Mobil Jangka Panjang Bulanan",
                "Gratis Antar - Jemput Mobil di Bandara",
                "Layanan Airport Transfer / Drop In Out",
              ].map((service, index) => (
                <li key={index} className="flex-sc gap-3 pb-3">
                  <Image src="/icon_checklist.svg" width={24} height={24} />
                  <p>{service}</p>
                </li>
              ))}
            </ul>
          </div>
        </section>

        <section id="WhyUs" className="max-w-screen-xl mx-auto py-3 px-3">
          <div className="pb-6 text-center md:text-left">
            <h2>Why Us?</h2>
            <p>Mengapa harus pilih Binar Car Rental?</p>
          </div>
          <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-6 mx-10 md:mx-0">
            {[
              [
                "/icon_complete.svg",
                "Mobil Lengkap",
                "Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat",
              ],
              [
                "/icon_price.svg",
                "Harga Murah",
                "Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain",
              ],
              [
                "/icon_24hrs.svg",
                "Layanan 24 Jam",
                "Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu",
              ],
              [
                "/icon_professional.svg",
                "Sopir Profesional",
                "Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu",
              ],
            ].map(([icon, headline, description], index) => (
              <div key={index} className="p-6 rounded-md border">
                <Image src={icon} width={32} height={32} />
                <h3>{headline}</h3>
                <p>{description}</p>
              </div>
            ))}
          </div>
        </section>

        <section className="max-w-screen-xl mx-auto py-3 px-3">
          <div className="w-full text-center bg-BLUE p-16 rounded-xl text-white">
            <h1>Sewa Mobil di Indonesia Sekarang</h1>
            <p className="pt-6 pb-4">
              Sewa mobil murah, terpercaya, dan anti-ribet se-Indonesia hanya di Binar Car Rental. Dapatkan promo menarik dan harga spesial dengan menyewa melalui situs resmi Binar Car Rental.
            </p>
            <a href="/sewa">
              <button className="btn">Mulai Sewa Mobil</button>
            </a>
          </div>
        </section>

        <section
          id="FAQ"
          className="max-w-screen-xl mx-auto py-3 px-3 grid grid-cols-1 md:grid-cols-12"
        >
          <div className="text-center md:text-left mb-4 md:col-span-5">
            <h2>Frequently Asked Question</h2>
            <p>Beberapa hal penting yang sering ditanyakan pengguna baru jasa kami</p>
          </div>
          <ul className="md:col-span-7">
            {[
              [
                "Apa saja syarat yang dibutuhkan?",
                "SIM, KTP, SKCK, Paspor, KK, dan Akta Kelahiran",
              ],
              ["Berapa hari minimal sewa mobil lepas kunci?", "12 hari"],
              [
                "Berapa hari sebelumnya sebaiknya booking sewa mobil?",
                "Booking dapat dilakukan 2 hari sebelum tanggal penyewaan. Namun untuk memastikan ketersediaan mobil, disarankan melakukan booking setidaknya 7 hari sebelum tanggal penyewaan.",
              ],
              ["Apakah ada biaya antar-jemput?", "Tidak ada"],
              [
                "Bagaimana jika terjadi kecelakaan?",
                "Jika kecelakaan terjadi akibat kelalaian penyewa dan tidak dapat tercover oleh asuransi, kerusakan sepenuhnya ditanggung penyewa. Jika kecelakaan terjadi bukan akibat kelalaian penyewa dan tidak dapat tercover oleh asuransi, kerusakan ditanggung 50% oleh penyewa dan 50% oleh kami.",
              ],
            ].map(([question, answer], index) => (
              <li key={index} className="border rounded mb-4 border-gray-300">
                <QnaAccordion question={question} answer={answer} />
              </li>
            ))}
          </ul>
        </section>
      </MainLayout>
    </>
  );
};

export default Home;
