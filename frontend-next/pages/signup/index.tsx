import type { NextPage } from "next";
import Head from "next/head";
import AuthLayout from "../../components/layout/authLayout";

const Signup: NextPage = () => {
  return (
    <>
      <Head>
        <title>Sign Up</title>
        <meta name="description" content="Rent A Car Anywhere Anytime" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <AuthLayout>
      <h2>Create a New Account</h2>
      </AuthLayout>
    </>
  );
};

export default Signup;
