import type { NextPage } from "next";
import Head from "next/head";
import HeroSection from "../../components/heroSection";
import MainLayout from "../../components/layout/mainLayout";
import axios from "axios";

const Cars: NextPage = (allcars: any = []) => {
  console.log(allcars);

  return (
    <>
      <Head>
        <title>BCR</title>
        <meta name="description" content="Rent A Car Anywhere Anytime" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <MainLayout>
        <HeroSection ishome={false} />

        <section className="grid grid-cols-4 gap-8 max-w-screen-xl mx-auto">
          {Object.values(allcars).map((car: any) => (
            <div className="border rounded-lg col flex-sc p-4 gap-2">
              <img className="w-full aspect-square object-contain " src={car.image} alt="car image" />
              <div className="w-full col gap-1">
                <p>{car.manufacture + " " + car.model}</p>
                <h3>{"Rp." + car.rentPerDay + " /hari"}</h3>
                <p>{car.description}</p>
                <p>{car.capacity}</p>
                <p>{car.transmission}</p>
                <p>{"Tahun "+car.year}</p>
              </div>
              <button className="btn w-full mt-auto">Pilih Mobil</button>
            </div>
          ))}
        </section>
      </MainLayout>
    </>
  );
};

Cars.getInitialProps = async (ctx) => {
  let API = process.env.NEXT_PUBLIC_API_ENDPOINT;
  let allcars = [];
  try {
    const res: any = await axios.get(API + "/main/data/cars.min.json");
    allcars = res.data;
  } catch (error: any) {
    console.log(error.response);
  }
  return allcars;
};

export default Cars;
