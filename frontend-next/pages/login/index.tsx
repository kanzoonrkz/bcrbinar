import type { NextPage } from "next";
import Head from "next/head";
import AuthLayout from "../../components/layout/authLayout";

const Login: NextPage = () => {
  return (
    <>
      <Head>
        <title>Log In</title>
        <meta name="description" content="Rent A Car Anywhere Anytime" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <AuthLayout>
        <h2>Welcome Back!</h2>
      </AuthLayout>
    </>
  );
};

export default Login;
