import Image from "next/image";
import React from "react";

function Footer() {
  return (
    <footer className="max-w-screen-xl mx-auto px-3 grid md:flex-bs gap-y-4 gap-x-4">
    {/* <footer className="max-w-screen-xl mx-auto px-3 grid grid-cols-1 md:grid-cols-4 gap-y-4 gap-x-4"> */}
      <div className="max-w-[250px] col gap-2">
        <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
        <p>binarcarrental@gmail.com</p>
        <p>081-233-334-808</p>
      </div>
      <ul className="font-medium md:pl-2 col gap-2">
        <li>
          <a href="/#OurServices">Our Services</a>
        </li>
        <li>
          <a href="/#WhyUs">Why Us</a>
        </li>
        <li>
          <a href="/#Testimonial">Testimonial</a>
        </li>
        <li>
          <a href="/#FAQ">FAQ</a>
        </li>
      </ul>
      <div>
        <p>Connect with us</p>
        <div className="flex gap-2 mt-2">
          {[
            ["/icon_facebook.svg", "facebook"],
            ["/icon_instagram.svg", "instagram"],
            ["/icon_twitter.svg", "twitter"],
            ["/icon_mail.svg", "email"],
            ["/icon_twitch.svg", "twitch"],
          ].map(([src,alt],index)=>(

        <Image key={index} src={src} alt={alt} height={32} width={32} />
          ))}
        </div>
      </div>
      <div className="">
      <p>Copyright Binar 2022</p>
      <div className="logo h-8 mt-2"></div>
    </div>
    </footer>
  );
}

export default Footer;
