import React from "react";

function HeroSection(props:{ishome:boolean}) {
  return (
    <section className="bg-BLUEsoft relative z-0">
      <div className="absolute bottom-0 right-0 w-[90vw] h-[30vw] md:w-[48vw] md:h-[55%] bg-BLUE rounded-tl-[60px] -z-10"></div>
      <div className="max-w-screen-2xl grid grid-cols-1 md:grid-cols-2 mx-auto ">
        <div className="py-3 px-3 my-auto md:ml-auto md:mr-[2vw]">
          <h1 className="max-w-xl mb-2">
            Sewa & Rental Mobil Terbaik di Indonesia
          </h1>
          <p className="max-w-lg mb-4">
            Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas
            terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu
            untuk sewa mobil selama 24 jam.
          </p>
          {props.ishome&&
          <a href="/cars">
            <button className="btn">Mulai Sewa Mobil</button>
          </a>
          }
          
        </div>
        <div className="pl-[10%] md:pl-[7%] my-auto">
          <img src="mobil.png" alt="car illustration" />
        </div>
      </div>
    </section>
  );
}

export default HeroSection;
