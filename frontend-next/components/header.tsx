import React from "react";

function Header() {
  return (
    <div className="sticky top-0 w-full bg-BLUEsoft z-50">
      <div className="max-w-screen-xl mx-auto flex-bc py-3 px-3">
        <div className="logo h-8 m-1"></div>
        <nav className=" gap-8 hidden md:flex-cc font-medium">
          <a href="/#OurServices">Our Services</a>
          <a href="/#WhyUs">Why Us</a>
          <a href="/#Testimonial">Testimonial</a>
          <a href="/#FAQ">FAQ</a>
          <button className="btn">Register</button>
        </nav>
        <button className="md:hidden">Toggle</button>
      </div>
    </div>
  );
}

export default Header;
