import {ReactNode} from 'react';
import Footer from '../footer';
import Header from '../header';

interface Props {
  children: ReactNode;
}
function mainLayout({children}:any) {
  return (
    <>
      <Header/>
      {children}
      <Footer/>
    </>
  )
}

export default mainLayout