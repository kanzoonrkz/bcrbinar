import { ReactNode } from "react";

interface Props {
  children: ReactNode;
}
function authLayout({ children }: any) {
  return (
    <div className="full grid grid-cols-2">
      <div className="col flex-cs">
        <div className="logo h-8 m-1 "></div>
        {children}
      </div>
      <div className="bg-BLUE h-[100vh] w-[50vw] flex-cc p-10">
        <h1 className="text-6xl font-bold text-white">Binar Car Rental</h1>
        <p className="text-9xl">🥰</p>
      </div>
    </div>
  );
}

export default authLayout;
