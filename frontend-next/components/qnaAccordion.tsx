import Image from "next/image";
import React, { useState } from "react";

function QnaAccordion({ question, answer, index }: any) {
  let [isExpand, setIsExpand] = useState(false);
  const toggle = () => {
    setIsExpand(!isExpand);
  };

  return (
    <button onClick={toggle} className="full py-4 px-6 group">
      <div className="flex-bc text-left">
        <p>{question}</p>
        <Image src="/faqArrow.svg" alt="more" height={18} width={18} />
      </div>
      <p className={`text-left pt-2 ${isExpand ? "block" : "hidden"}`}>
        {answer}
      </p>
    </button>
  );
}

export default QnaAccordion;
