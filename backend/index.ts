import express, { Request, Response } from "express";
const PORT = process.env.port || 8080;
import { PrismaClient } from "@prisma/client";
import { resolve } from "path";
import { rejects } from "assert";
const bcrypt = require("bcryptjs");
const salt = 10;
const app = express();

app.use(express.json());
const prisma = new PrismaClient();

app.post("/", async (req: Request, res: Response) => {
  const {
    plate,
    manufacture,
    model,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    image,
    options,
    specs,
  } = req.body;
  const newCar = await prisma.car.create({
    data: {
      plate: plate,
      manufacture: manufacture,
      model: model,
      rentPerDay: rentPerDay,
      capacity: capacity,
      description: description,
      transmission: transmission,
      available: available,
      type: type,
      year: year,
      image: image,
      options: options,
      specs: specs,
    },
  });
  res.json(newCar);
});

app.post("/createMany", async (req: Request, res: Response) => {
  const { newCarList } = req.body;
  const newCars = await prisma.car.createMany({
    data: newCarList,
  });
  res.json(newCars);
});

app.get("/", async (req: Request, res: Response) => {
  const cars = await prisma.car.findMany();
  res.json(cars);
});

app.get("/:id", async (req: Request, res: Response) => {
  const id = req.body;
  const cars = await prisma.car.findUnique({
    where: {
      id: Number(id),
    },
  });
  res.json(cars);
});

app.put("/", async (req: Request, res: Response) => {
  const {
    id,
    plate,
    manufacture,
    model,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    image,
    options,
    specs,
  } = req.body;
  const editedCar = await prisma.car.update({
    where: {
      id: id,
    },
    data: {
      plate: plate,
      manufacture: manufacture,
      model: model,
      rentPerDay: rentPerDay,
      capacity: capacity,
      description: description,
      transmission: transmission,
      available: available,
      type: type,
      year: year,
      image: image,
      options: options,
      specs: specs,
    },
  });
  res.json(editedCar);
});

app.delete("/:id", async (req: Request, res: Response) => {
  const id = req.params.id;
  const deletedUser = await prisma.car.delete({
    where: {
      id: Number(id),
    },
  });
  res.json(deletedUser);
});

async function encryptPassword(password:string) {
  const newPasswd = await new Promise((resolve, rejects) => {
    bcrypt.hash(password, salt, ({err, encryptPassword}:any) => {
      if (!!err) {
        rejects(err);
        return;
      }
      resolve(encryptPassword);
    });
  });
  return newPasswd;
}

async function checkPassword(encryptPassword:string, password:string) {
  const compared = await new Promise((resolve, rejects) => {
    bcrypt.compare(password, encryptPassword, ({err, isPasswordCorrect}:any) => {
      if (!!err) {
        rejects(err);
        return;
      }
      resolve(isPasswordCorrect);
    });
  });
  return compared;
}

app.post("/v1/register", async (req: Request, res: Response) => {
  const hashedPassword = await encryptPassword(req.body.password).toString()
  const {email,name,username} = req.body;
  const newUser = await prisma.user.create({
    data:{
      email: email.toLowerCase(),
      name: name,
      username: username,
      password: hashedPassword
    }
  });
  res.json(newUser);
});

app.post("/v1/login", async (req: Request, res: Response) => {
  const {email,password} = req.body;
  const user = await prisma.user.findUnique({
    where: {
      email: email,
    },
  });
  if (!user) {
    return res.status(404).json({
      error: "Email not found"
    });
  }
  const hashedPassword = await encryptPassword(req.body.password).toString()
  const isPasswordCorrect = await checkPassword(user.password, hashedPassword);
  if(!isPasswordCorrect){
    return res.status(401).json({
      error: "Wrong password"
    });
  }
  res.status(201).json(user);
});

app.listen(PORT, () => console.log(`Server listening on port: ${PORT}`));
